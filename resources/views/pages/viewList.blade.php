@extends('base')
@section('main')

<div class="row">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    .homebtn {
      margin-top: 12px;
      margin-left: 30px;
      background-color: #6bb5ff;
      border: none;
      color: black;
      float: left;
      padding: 14px 18px;
      font-size: 16px;
      cursor: pointer;
      border-radius: 5px;
    }

    /* Darker background on mouse-over */
    .homebtn:hover {
      background-color: #085099;
      color: white;
    }
    .btn-btn-primary {
      border: 20px DodgerBlue;
      background-color: #6bb5ff;
      color: black;
      padding: 14px 28px;
      font-size: 18px;
      cursor: pointer;
      float:right;
      border-radius: 5px;
    }
    .btn-btn-primary:hover {
      background-color: #085099;
      color: white;
      text-decoration: none;
    }
  </style> 
</div>

<div class="row">
<a href="http://192.168.10.10/" class="homebtn"><i class="fa fa-home" ></i></a>
  <a style="margin: 15px;"  href="http://192.168.10.10/blog/insert" class="btn-btn-primary">Create New Blog Post</a>
</div> 

div class="col-sm-12">
  <h1 class="display-3">Blogs</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Title</td>
          <td>Content</td>
          <td>Writer</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $blog)
        <tr>
            <td>{{$blog->id}}</td>
            <td>{{$blog->updated_at}}
            <td>{{$blog->title}}</td>
            <td>{{$blog->content}}</td>
            <td>{{$blog->writer}}</td>
            <td>
            <a  href=" http://127.0.0.1:8000/blog/edit/{{$blog->id}}" data-method="post"  class="btn btn-primary">Edit</a>
            </td>
            <td>
            <form method="POST" action=" http://127.0.0.1:8000/blog/destroy/{{$blog->id}}">
            <input name="_method" type="hidden" value="DELETE">
            <input class="btn btn-danger" type="submit" value="Delete">
            </form>
            </td>   
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
@endsection
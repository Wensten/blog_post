@extends('base')
@section('main')

<div class="row">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
     .homebtn {
      margin-top: 12px;
      margin-left: 30px;
      background-color: #6bb5ff;
      border: none;
      color: black;
      float: left;
      padding: 14px 18px;
      font-size: 16px;
      cursor: pointer;
      border-radius: 5px;
    }

    /* Darker background on mouse-over */
    .homebtn:hover {
      background-color: #085099;
      color: white;
    }
    .btn-btn-primary , .btn-btn-primary-outline {
      border: 20px DodgerBlue;
      background-color: #6bb5ff;
      color: black;
      padding: 14px 28px;
      font-size: 18px;
      cursor: pointer;
      float:right;
      border-radius: 5px;
    }

    .btn-btn-primary-outline{
      float:left;
    }

    .btn-btn-primary:hover , .btn-btn-primary-outline:hover{
      background-color: #085099;
      color: white;
      text-decoration: none;
    }
  </style>
</div>

<div class="row">
  <a style="margin: 15px;"  href="http://127.0.0.1:8000/employee/" class="btn-btn-primary">Back to Posts List</a>
</div> 



  <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create Post</h1>
    <div>
      <form action="http://127.0.0.1:8000/blog/create" method="post">
          <div class="form-group">    
              <label for="title">Title:</label>
              <input type="text" class="form-control" name="title" required/>
          </div>

          <div class="form-group">
              <label for="content">About the post:</label>
              <input type="text" class="form-control" name="content" required/>
          </div>

          <div class="form-group">
              <label for="writer">Writer Name:</label>
              <input type="text" class="form-control" name="writer" required/>
          </div>                    
          <button type="submit" class="btn-btn-primary-outline">Add Post</button>
      </form>
    </div>
  </div>
@endsection
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\blogsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/blog', [blogsController::class, 'index']); 


Route::post('/blog/create', [blogsController::class, 'create']); 


Route::get('/blog/show/{id}', [blogsController::class, 'show']); 


Route::post('/blog/edit/{id}', [blogsController::class, 'edit']); 


Route::delete('/blog/destroy/{id}', [blogsController::class, 'destroy']); 


Route::get('/blog/insert', function () { return view('pages.create'); });
    

Route::get('/blog/show/{id}', function () { return view('pages.update'); });